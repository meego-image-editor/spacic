/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
//** QIntersect ***********************************************************
//** This class implements Intersect query, given two data inputs.       **
//*************************************************************************

#include "Query.h"
#include "Rect.h"
#include <iostream>
#include <string.h>

int (*QIntersect::ItemsEstimator)(int Min,int Max) = Query::AvgItems;
// By setting this pointer to MinItems, MaxItems, AvgItems, or some
// other estimator function, the user can control the return value
// of the Items() call.

QIntersect::QIntersect(Query *NewInput1,Query *NewInput2)
{
  XType   = QU_INTERSCT;
  XInput1 = NewInput1;
  XInput2 = NewInput2;

  (Rect &)*this=(Rect &)*NewInput1*(Rect &)*NewInput2;
}

Region *QIntersect::GetFirst(QContext &Q,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  QContext TempQ;
  Region *R1,*R2;
  int D1,D2;

  // Starting from scratch
  Q.Reset(Type(),1);
  D1=XInput1->Cost();
  D2=XInput2->Cost();
  C->CurInput = D1>=D2? XInput2:XInput1;
  C->AltInput = D1>=D2? XInput1:XInput2;

  // Get first region from the first input
  R1=C->CurInput->GetFirst(Q.In(),&D1);

  // If we got an explicit region, check for occurance
  if(R1&&!D1)
  {
    // Check if explicit region occurs in the second input
    R2=C->AltInput->GetFirst(TempQ,&D2);
    for(;R2;R2=C->AltInput->GetNext(TempQ,D2,&D2))
    {
      if(!D2&&R2->Contains(*R1)&&R1->Contains(*R2)) break;
      if(D2&&R2->Contains(*R1)) D2=0;
    }

    // If region not found in the second input, get next region
    if(!R2) R1=GetNext(Q,0,&D1);
  }

  if(R1) *OutDegree=D1; else Q.Reset();
  return(R1);
}

Region *QIntersect::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  QContext TempQ;
  Region *R1,*R2;
  int D1,D2;

  // Out of regions, need to call GetFirst() to restart
  if(Q.Type()!=Type()) return(0);

  do
  {
    // Get next region from the first input
    R1=C->CurInput->GetNext(Q.In(),InDegree,&D1);

    // Fall out if out of regions
    // Return implicit regions right away
    if(!R1||D1) break;

    // Check if explicit region occurs in the second input
    R2=C->AltInput->GetFirst(TempQ,&D2);
    for(;R2;R2=C->AltInput->GetNext(TempQ,D2,&D2))
    {
      if(!D2&&R2->Contains(*R1)&&R1->Contains(*R2)) break;
      if(D2&&R2->Contains(*R1)) D2=0;
    }
  }
  while(!R2);

  if(R1) *OutDegree=D1; else Q.Reset();
  return(R1);
}

int QIntersect::Items(const Region &R) const
{
  int C1,C2;
  Region *R1,*R2;

  R1=R2=0;
  C1=XInput1->Intersect(R,&R1,1);
  C2=XInput2->Intersect(R,&R2,1);

  // If it is an abnormal case...
  if((C1<=0)||(C2<=0))
  {
    // Delete regions
    if(R1) delete R1;
    if(R2) delete R2;

    // If error occured, relay it up
    if((C1<0)||(C2<0)) return(-1);

    // No intersection, 0 items
    return(0);
  }

  // Everything is ok, compute item counts
  C1=XInput1->Items(*R2);
  C2=XInput2->Items(*R1);

  // Delete regions
  delete R1;
  delete R2;

  // If error occured, relay it up
  if((C1<0)||(C2<0)) return(-1);

  // Items() is in 0..min(C1,C2) range
  return((*ItemsEstimator)(0,C1<C2? C1:C2));
}

int QIntersect::Cost(const Region &R) const
{
  int Res,C1,C2;
  Region *R1,*R2;

  R1=R2=0;
  C1=XInput1->Intersect(R,&R1,1);
  C2=XInput2->Intersect(R,&R2,1);

  if((C1<=0)||(C2<=0))
  {
    // Delete regions
    if(R1) delete R1;
    if(R2) delete R2;

    // If error occured, relay it up
    if((C1<0)||(C2<0)) return(-1);

    // No intersection, first term of the sum is 0
    Res=0;
  }
  else
  {
    // Everything is ok, compute first term of the sum
    C1=XInput1->Items(*R2);
    C2=XInput2->Cost(*R1);
    Res=(C1>=0)&&(C2>=0)? (C1*C2):-1;
    
    // Delete regions
    delete R1;
    delete R2;
  }

  // Compute second term of the sum
  if(Res>=0)
  {
    C1=XInput1->Cost(R);
    Res=(C1>=0)? (Res+C1):-1;
  }

  // Done
  return(Res);
}
