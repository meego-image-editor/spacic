/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
// 
// LIMTOTAL ITEMS() ALGORITHM:
// This is the totally bounded cost estimation algorithm.
//
  TNode *NBuf[DecompSetSize+4];
  int Res,J,Head,Tail;
  Region *P;
  TNode *N;

  if(!Root||!R.Intersects(Root->Imp)) return(0);

  // No items yet, buffer only contains root node
  Res=0;Tail=1;NBuf[0]=Root;

  for(Head=0;(Tail<DecompSetSize)&&(Head<Tail);Head++)
  {
    N=NBuf[Head];

    if(R.Contains(N->Imp)) { Res+=N->ItemCount;NBuf[Head]=0; }
    else
    {
      // Try adding regions to the tail
      J=Tail;
      if(N->NW&&R.Intersects(N->NW->Imp)) NBuf[J++]=N->NW;
      if(N->NE&&R.Intersects(N->NE->Imp)) NBuf[J++]=N->NE;
      if(N->SW&&R.Intersects(N->SW->Imp)) NBuf[J++]=N->SW;
      if(N->SE&&R.Intersects(N->SE->Imp)) NBuf[J++]=N->SE;

      // If successful, shift tail
      if(J<=DecompSetSize) { NBuf[Head]=0;Tail=J; }
    }
  }

  for(J=0;J<Tail;J++)
    if(NBuf[J])
      if(NBuf[J]->Imp.Intersect(R,&P,1)<=0) return(-1);
      else
      {
        // This does not allow zero-area regions
        if(NBuf[J]->Imp.Area()>P->Area())
          Res+=(int)(NBuf[J]->ItemCount*P->Area()/NBuf[J]->Imp.Area());
        // No longer need intersection result
        delete P;
      }

  // Done
  return(Res);
