/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "GNIS.h"
#include "Attrs.h"
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>

#define PI 3.14159265

#ifdef ZLIB
#include <zlib.h>
#define fopen          (FILE *)gzopen
#define fclose         gzclose
#define fread(B,N,L,F) gzread(F,B,(L)*(N))
#define fseek          gzseek
#define rewind         gzrewind
#define fgetc          gzgetc
#define fgets(S,N,F)   gzgets(F,S,N)
#endif

GNIS::GNIS(const char *NewFileName,double NewScale,double NewX,double NewY,const char *NewName)
  : QData(NewName)
{
  FileName=new char[strlen(NewFileName)+1];
  if(FileName) strcpy(FileName,NewFileName);
  ItemCount=-1;
  Scale=NewScale;
  X=NewX;
  Y=NewY;
  F=0;
}

GNIS::~GNIS()
{
  if(F) fclose(F);
  if(FileName) delete [] FileName;
  FileName=0;
  F=0;
}

double GNIS::Digits(const char *S,int N) const
{
  int J;
  for(J=0;N;N--,S++) J=J*10+(*S-'0');
  return((double)J);
}

int GNIS::Items(const Region &R) const
{
  // @@@ ADD STUFF HERE!
  return(-1);
}

int GNIS::Cost(const Region &R) const
{
  FILE *F;
  int C,I;

  // If item count was computed before, return it
  if(ItemCount>=0) return(ItemCount);

  // No filename -> no cost
  if(!FileName) return(0);
  F=fopen(FileName,"rb");
  if(!F) return(0);

  // Count number of characters in the first line
  for(C=fgetc(F),I=0;(C>=0)&&(C!='\n');C=fgetc(F)) I++;
  // Accomodate for EOLN
  if(C>=0) I++;

  // Find number of characters in the file and
  // approximate number of lines
  fseek(F,0,SEEK_END);
  I=ftell(F)/I;

  // Done
  fclose(F);
  return(I);
}

Region *GNIS::GetFirst(QContext &Q,int *OutDegree)
{
  // No filename -> no data
  if(!FileName) return(0);

  // Open file if closed, rewind otherwise
  if(F) rewind(F); else F=fopen(FileName,"rb");

  // Reset current item counter
  CurCount=0;

  // And get the next record
  return(GetNext(Q,0,OutDegree));
}

Region *GNIS::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  char S[1024];
  char T[1024],*P;
  double Lat,Lon;
  int J;

  // No file -> no data
  if(!F) return(0);

  while(fgets(S,sizeof(S)-1,F))
  {
    // Check lat/lon for correct syntax
    for(J=149;J<164;J++)
      if((J==155)||(J==163)) { if(!isalpha(S[J])) break; }
      else { if(!isdigit(S[J])) break; }

    if(J<164)
    {
      // Error in lat/lon
      S[164]='\0';
    }
    else  
    {
      // Prepare attributes
      P=T;
      strcpy(P,"Name");P+=strlen(P)+1;
      memcpy(P,S,100);for(P+=100;P[-1]==' ';P--);*P++='\0';
      strcpy(P,"Type");P+=strlen(P)+1;
      memcpy(P,S+100,9);for(P+=9;P[-1]==' ';P--);*P++='\0';
      strcpy(P,"County");P+=strlen(P)+1;
      memcpy(P,S+109,35);for(P+=35;P[-1]==' ';P--);*P++='\0';
      strcpy(P,"ZIP");P+=strlen(P)+1;
      memcpy(P,S+144,5);for(P+=5;P[-1]==' ';P--);*P++='\0';
      strcpy(P,"City");P+=strlen(P)+1;
      memcpy(P,S+185,53);for(P+=53;P[-1]==' ';P--);*P++='\0';
      *P='\0';

      // Parse lat/lon
      Lat=Digits(S+149,2)+Digits(S+151,2)/60.0+Digits(S+153,2)/3600.0;
      Lat=PI*Lat/180.0;
      if(S[155]=='S') Lat=-Lat;
      Lon=Digits(S+156,3)+Digits(S+159,2)/60.0+Digits(S+161,2)/3600.0;
      Lon=PI*Lon/180.0;
      if(S[163]=='W') Lon=-Lon;

      // Project lat/lon to map coordinates
      Lon=Scale*(Lon+Y);
      Lat=PI/4.0+Lat/2.0;
      Lat=Scale*(log(tan(Lat))+X);

      // Generate region
      CurData=Point(Lon,Lat);
      CurCount++;

      // Store attributes
      CurData.User=AttrBase.Set(CurData,T);

      // Return point
      *OutDegree=0;
      return(&CurData);
    }
  }

  // Out of data, close file and assign item count
  ItemCount=CurCount;
  fclose(F);
  F=0;
  return(0);
}
