/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
//** QUnion ***************************************************************
//** This class implements Union query, given two data inputs.           **
//*************************************************************************

#include "Query.h"
#include "Rect.h"
#include <iostream>
#include <string.h>

int (*QUnion::ItemsEstimator)(int Min,int Max) = Query::AvgItems;
// By setting this pointer to MinItems, MaxItems, AvgItems, or some
// other estimator function, the user can control the return value
// of the Items() call.

QUnion::QUnion(Query *Z1,Query *Z2)
{
  XType    = QU_UNION;
  XInput1  = Z1;
  XInput2  = Z2;

  (Rect &)*this=(Rect &)*Z1+(Rect &)*Z2;
}

Region *QUnion::GetFirst(QContext &Q,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  QContext TempQ;
  Region *R1,*R2;
  int D1,D2;

  // Starting from scratch
  Q.Reset(Type(),1);
  D1=XInput1->Cost();
  D2=XInput2->Cost();
  C->CurInput  = D1>=D2? XInput2:XInput1;
  C->AltInput  = D1>=D2? XInput1:XInput2;
  C->AltAdjust = 0;
  C->CurAdjust = 0;

  // Currently reading from the first input
  R1=C->CurInput->GetFirst(Q.In(),&D1);

  if(R1)
  {
    // Start scan for duplicates in the second dataset
    R2=C->AltInput->GetFirst(TempQ,&D2);

    // Figure out degree adjustments
    if(R2) { if(D1>D2) C->AltAdjust=D1-D2; else C->CurAdjust=D2-D1; }

    // Return implicit region immediately
    if(D1) { *OutDegree=D1+C->CurAdjust;return(R1); }

    // Check if explicit region belongs to the second dataset
    // If duplicate found, skip current R1 by calling GetNext()
    for(;R2;R2=C->AltInput->GetNext(TempQ,D2,&D2))
    {
      if(!D2&&R2->Contains(*R1)&&R1->Contains(*R2))
        return(GetNext(Q,0,OutDegree));
      if(D2&&R2->Contains(*R1)) D2=0;
    }

    // Return explicit region
    *OutDegree=0;
    return(R1);
  }

  C->CurInput = XInput2;
  C->AltInput = 0;

  // Currently reading from the second input
  R1=C->CurInput->GetFirst(Q.In(),&D1);

  if(R1) *OutDegree=D1; else Q.Reset();
  return(R1);
}

Region *QUnion::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  QContext TempQ;
  Region *R1,*R2;
  int D1,D2,Flag;

  // Out of regions, need to call GetFirst() to restart
  if(Q.Type()!=Type()) return(0);

  // Get next data from CurInput, no switch to AltInput occured
  Flag=0;

  do
  {
    if(!Flag) R1=C->CurInput->GetNext(Q.In(),InDegree-C->CurAdjust,&D1);
    else
    {
      R1=C->CurInput->GetFirst(Q.In(),&D1);
      Flag=0;

      if(!R1||(D1<InDegree-C->CurAdjust))
      {
        C->CurInput=0;
        R1=0;
      }
    }

    // If ran out of regions...
    if(!R1)
    {
      // Switch to the second input
      C->CurAdjust  = C->AltAdjust;
      C->CurInput   = C->AltInput;
      C->AltInput   = 0;

      // Do a GetFirst() at the next pass
      Flag=1;
    }
    else
    {
      // Return implicit regions immediately
      if(D1) { *OutDegree=D1+C->CurAdjust;return(R1); }

      // So far, we wish to return R1...
      Flag=1;

      // Check for duplicates in the second input
      if(C->AltInput)
      {
        // Check if explicit region belongs to the second dataset
        R2=C->AltInput->GetFirst(TempQ,&D2);
        for(;R2;R2=C->AltInput->GetNext(TempQ,D2,&D2))
        {
          if(!D2&&R2->Contains(*R1)&&R1->Contains(*R2)) { Flag=0;break; }
          if(D2&&R2->Contains(*R1)) D2=0;
        }
      }

      // Return explicit region
      if(Flag) { *OutDegree=0;return(R1); }
    }
  }
  while(C->CurInput);

  // Out of data
  Q.Reset();
  return(0); 
}

int QUnion::Items(const Region &R) const
{
  int C1,C2;

  C1=XInput1->Items(R);
  C2=XInput2->Items(R);
  if((C1<0)||(C2<0)) return(-1);

  // Items() is in max(C1,C2)...C1+C2 range
  return((*ItemsEstimator)(C1>C2? C1:C2,C1+C2));
}

int QUnion::Cost(const Region &R) const
{
  int C1,C2,Res;
  Region *R1,*R2;

  R1=R2=0;
  C1=XInput1->Intersect(R,&R1,1);
  C2=XInput2->Intersect(R,&R2,1);

  // If it is an abnormal case...
  if((C1<=0)||(C2<=0))
  {
    // Delete regions
    if(R1) delete R1;
    if(R2) delete R2;

    // If error occured, relay it up
    if((C1<0)||(C2<0)) return(-1);

    // No intersection, first two terms of the sum are 0
    Res=0;
  }
  else
  {
    // Everything is ok, compute first term of the sum
    C1=XInput1->Items(*R2);
    C2=XInput2->Cost(*R1);
    Res=(C1>=0)&&(C2>=0)? (C1*C2):-1;

    // Compute second term of the sum
    if(Res>=0)
    {
      C1=XInput2->Items(*R1);
      C2=XInput1->Cost(*R2);
      Res=(C1>=0)&&(C2>=0)? (Res+C1*C2):-1;
    }

    // Delete regions
    delete R1;
    delete R2;
  }

  // Add up third and fourth terms
  if(Res>=0)
  {
    C1=XInput1->Cost(R);
    C2=XInput2->Cost(R);
    Res=(C1>=0)&&(C2>=0)? (Res+C1+C2):-1;
  }

  // Done
  return(Res);
}
