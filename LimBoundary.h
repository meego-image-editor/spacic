/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
//
// LIMBOUNDARY ITEMS() ALGORITHM:
// This is the partially bounded cost estimation algorithm. It allows to
// put a limit on the number of nodes that lie at the region boundary.
//
  TNode *NBuf[DecompSetSize];
  int J,K,Res,Head,Tail,Sca;
  Region *P;
  TNode *N;

  // No intersection or no root give us 0 items
  if(!Root||!R.Intersects(Root->Imp)) return(0);

  NBuf[0]=Root; // Start scanning from the root node
  Head=0;       //
  Tail=1;       //
  J=Tail;       // Add new items at the tail
  Res=0;        // No items yet
  Sca=0;        // Nothing scanned yet

  for(;Head!=Tail;Head=NextI(Head))
  {
    N=NBuf[Head]; // We look at a node located in the queue head
    Sca++;        //
    J=Tail;       // Add new nodes to the queue tail
    K=1;          // This solitary data item has been stored in N itself

    if(N->NW)
      if(R.Contains(N->NW->Imp)) { K+=N->NW->ItemCount;Sca++; }
      else
        if(R.Intersects(N->NW->Imp))
          if(J==Head) break;
          else { NBuf[J]=N->NW;J=NextI(J); }

    if(N->NE)
      if(R.Contains(N->NE->Imp)) { K+=N->NE->ItemCount;Sca++; }
      else
        if(R.Intersects(N->NE->Imp))
          if(J==Head) break;
          else { NBuf[J]=N->NE;J=NextI(J); }

    if(N->SW)
      if(R.Contains(N->SW->Imp)) { K+=N->SW->ItemCount;Sca++; }
      else
        if(R.Intersects(N->SW->Imp))
          if(J==Head) break;
          else { NBuf[J]=N->SW;J=NextI(J); }

    if(N->SE)
      if(R.Contains(N->SE->Imp)) { K+=N->SE->ItemCount;Sca++; }
      else
        if(R.Intersects(N->SE->Imp))
          if(J==Head) break;
          else { NBuf[J]=N->SE;J=NextI(J); }

    Tail=J; // Update tail pointer
    Res+=K; // Update item count
  }

  // All nodes remaining in the buffer will be scanned
  Sca+=(Head<=Tail)? (Tail-Head):(DecompSetSize-Head+Tail);

  // Scan all nodes left in the buffer
  for(;Head!=Tail;Head=NextI(Head))
  {
    N=NBuf[Head];

    if(N->Imp.Intersect(R,&P,1)<=0) return(-1);
    else
    {
      // This tests against zero-area regions
      if(N->Imp.Area()<=P->Area()) Res+=N->ItemCount;
      else Res+=(int)(N->ItemCount*P->Area()/N->Imp.Area());

      // No longer need intersection result
      delete P;
    }
  }

cout<<"Scanned "<<Sca<<" nodes"<<endl;

  // Done
  return(Res);
