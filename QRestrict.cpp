/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
//** QRestrict ************************************************************
//** This class implements Region Restriction query, given a data input  **
//** and a well formed formula with one unbound variable.                **
//*************************************************************************

#include "Query.h"
#include "Rect.h"
#include <iostream>
#include <string.h>

int (*QRestrict::ItemsEstimator)(int Min,int Max) = Query::MaxItems;
// By setting this pointer to MinItems, MaxItems, AvgItems, or some
// other estimator function, the user can control the return value
// of the Items() call.

QRestrict::QRestrict(Query *NewInput,WFF *NewCond,Region *NewVar)
{
  Region *Buf[CoverBufSize];
  int J,N;

  XType  = QU_RESTRICT;
  XInput = NewInput;
  XCond  = NewCond;
  XVar   = NewVar;
  XMono  = NewCond->Mono(NewVar);
  XAnti  = NewCond->Anti(NewVar);

  // Find areas covered by the query
  *XVar=(Rect &)*XInput;
  N=XCond->Eval(XVar,Buf,CoverBufSize);
  
  // Compound all areas into a single rectangle
  if(N<0) (Rect &)*this=(Rect &)*XInput;
  else
    for(J=0;J<N;J++)
    { 
      (Rect &)*this+=*(Rect *)Buf[J];
      delete Buf[J];
    }
}

Region *QRestrict::GetFirst(QContext &Q,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  Region *R;
  int D;

  // Starting from scratch
  Q.Reset(Type(),1);
  C->Items   = 0;
  C->Current = 0;

  // Get first region from the input
  R=XInput->GetFirst(Q.In(),&D);

  // Fall out if input is empty
  if(!R) { Q.Reset();return(0); }

  // Return implicit regions right away
  if(D>0) { *OutDegree=D;return(R); }

  // Evaluate condition storing result in C->Buffer
  *XVar=*R;
  D=XCond->Eval(XVar,C->Buffer,BufSize);

  // If condition returned sensible output, return first region
  if(D>0)
  {
    C->Items   = D;
    C->Current = 0;
    *OutDegree = 0;
    return(C->Buffer[0]);
  }

  // No sensible data returned, proceed with GetNext()
  return(GetNext(Q,0,OutDegree));
}

Region *QRestrict::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  Region *R;
  int D;

  // Out of regions, need to call GetFirst() to restart
  if(Q.Type()!=Type()) return(0);

  // If we have buffered regions...
  if(C->Current<C->Items)
  {
    // Delete previously returned region
    delete C->Buffer[C->Current++];
    // If we still have buffered regions...
    if(C->Current<C->Items)
    {
      // Return next explicit region (buffered) if requested
      if(InDegree<=0) { *OutDegree=0;return(C->Buffer[C->Current]); }
      // Otherwise, delete all buffered regions
      do delete C->Buffer[C->Current++]; while(C->Current<C->Items);
    }
  }

  do
  {
    // Get next region from the input
    R=XInput->GetNext(Q.In(),InDegree,&D);

    // Fall out if out of data
    if(!R) { Q.Reset();return(0); }

    // Return implicit regions right away, analyze epxlicit regions
    if(D>0) *OutDegree=D;
    else
    {    
      // Evaluate condition storing result in C->Buffer
      *XVar=*R;
      D=XCond->Eval(XVar,C->Buffer,BufSize);

      // If condition returned sensible output, return first region
      if(D>0)
      {
        C->Items   = D;
        C->Current = 0;
        *OutDegree = 0;
        R=C->Buffer[0];
      }
    }
  }
  while(D<=0);

  // Done
  return(R);
}

int QRestrict::Items(const Region &R) const
{
  int Res;  
  Res=XInput->Items((Rect)Intersect(R));
  return(Res<0? -1:(*ItemsEstimator)(0,Res));
}

int QRestrict::Cost(const Region &R) const
{
  int Res;  
  Res=XInput->Cost((Rect)Intersect(R));
  return(Res);
}
