/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef RTREE_H
#define RTREE_H

#include "Query.h"
#include "Rect.h"

#define REGS_PER_NODE 8

class RTree: public QData
{
  public:
    RTree(const char *NewName=0);
    ~RTree();

    virtual int Add(const Region &R);
    virtual int Delete(const Region &R);
    virtual int Cost(const Region &R) const;
    virtual int Items(const Region &R) const;
    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

  private:
    class TNode
    {
      public:
        TNode() { UseMask=0;Leaf=1;Parent=0; }
        ~TNode();
        Rect Data[REGS_PER_NODE];
        TNode *Child[REGS_PER_NODE];
        TNode *Parent;
        int UseMask;
        int Leaf;
    };
    class QCType
    {
      public:
        TNode *LastTNode;
        int LastDegree;
        int LastState;
    };

    TNode *Root;
    int Depth;

    TNode *ChooseSubtree(const Rect *R);
    void Split(const TNode *T,const Rect &R) const;
    double ComputeOver(const Rect &R1,const Rect &R2) const;
    double ComputeArea(const Rect &R1,const Rect &R2) const;
    Rect BoundingBox(const TNode *T) const;
};

#endif /* RTREE_H */
